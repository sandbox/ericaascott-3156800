CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

There are some newer vitals (Largest Contentful Paint, First Input Delay, Cumulative Layout Shift)
that are being tracked by Google, more information can be found here: https://web.dev/vitals

This module adds the ability to track this information in real-time across your website.
This is not intended to be used for development purposes and therefore
never used in a production environment.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
-------------

 * Navigate to /admin/config/development/web_vitals to enable the functionality

MAINTAINERS
-----------

Current maintainers:
 * Erica Wright (ericawright) - https://www.drupal.org/u/ericawright
