/**
 * @file
 * Web Vitals JS.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.drupalWebVitals = {
    attach: function (context, settings) {
      this.buildVitalsDisplay();
      webVitals.getLCP(this.displayVital, true);
      webVitals.getFID(this.displayVital);
      webVitals.getCLS(this.displayVital, true);

      const $header = $('.web-vitals .web-vitals__header');
      const $content = $('.web-vitals .web-vitals__content');

      // Toggle the content.
      $header.click(() => {
        $content.toggleClass('hidden');
        $header.toggleClass('open');
      });
    },
    // Build a block to display the results.
    buildVitalsDisplay() {
      let vitalsDisplay = '<div class="web-vitals">';
      vitalsDisplay += '<div class="web-vitals__header"><span class="title">Web Vitals</span><span class="caret down"></span></div>';
      vitalsDisplay += '<div class="web-vitals__content hidden">';
      vitalsDisplay += '<div class="web-vitals__content__LCP">LCP: <span class="web-vitals__content__value"></span></div>';
      vitalsDisplay += '<div class="web-vitals__content__FID">FID: <span class="web-vitals__content__value"></span></div>';
      vitalsDisplay += '<div class="web-vitals__content__CLS">CLS: <span class="web-vitals__content__value"></span></div>';
      vitalsDisplay += '<div class="web-vitals__content__message">To remove this block, disable it <a href="/admin/config/development/web_vitals">here</a>. For more information about these metrics, click <a href="https://web.dev/vitals/">here</a>.</div>';
      vitalsDisplay += '</div></div>';
      $('body').append(vitalsDisplay);
    },
    // Display the vital when it arrives in the block.
    displayVital(metric) {
      const {
        web_vitals
      } = drupalSettings;
      const { thresholds } = web_vitals;

      const $value = $('.web-vitals .web-vitals__content')
        .find(".web-vitals__content__" + metric.name)
        .find('span.web-vitals__content__value');

      $value.text(metric.value);

      if (metric.value < thresholds[metric.name].good) {
        $value.addClass('good');
        $value.removeClass('poor');
      } else if (metric.value > thresholds[metric.name].poor) {
        $value.addClass('poor');
        $value.removeClass('good');
      } else {
        $value.removeClass('good');
        $value.removeClass('poor');
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
