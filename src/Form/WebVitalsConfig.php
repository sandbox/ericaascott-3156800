<?php

namespace Drupal\web_vitals\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Web Vitals.
 */
class WebVitalsConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_vitals_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      WEB_VITALS_CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(WEB_VITALS_CONFIG_KEY);

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('When this is enabled, users with the correct permissions will see web vitals information on each page. Caution: This should not be used in production environments!'),
      '#default_value' => $config->get('enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(WEB_VITALS_CONFIG_KEY);
    $config->set('enabled', $form_state->getValue('enabled'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
